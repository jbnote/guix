;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2022 Alex Griffin <a@ajgrf.com>
;;; Copyright © 2022 Mathieu Othacehe <othacehe@gnu.org>
;;; Copyright © 2022 dan <i@dan.games>
;;;
;;; This file is part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (gnu system images wsl2)
  #:use-module (gnu bootloader)
  #:use-module (gnu image)
  #:use-module (gnu packages admin)
  #:use-module (gnu packages base)
  #:use-module (gnu packages bash)
  #:use-module (gnu packages guile)
  #:use-module (gnu packages linux)
  #:use-module (gnu services)
  #:use-module (gnu services base)
  #:use-module (gnu system)
  #:use-module (gnu system image)
  #:use-module (gnu system shadow)
  #:use-module (guix build-system trivial)
  #:use-module (guix gexp)
  #:use-module (guix packages)
  #:use-module (gnu packages)
  #:use-module ((guix licenses) #:select (fsdg-compatible))
  #:export (wsl-boot-program
            wsl-os
            wsl2-image))

(define dummy-package
  (package
    (name "dummy")
    (version "0")
    (source #f)
    (build-system trivial-build-system)
    (arguments
     `(#:modules ((guix build utils))
       #:target #f
       #:builder (begin
                   (use-modules (guix build utils))
                   (let* ((out (assoc-ref %outputs "out"))
                          (dummy (string-append out "/dummy")))
                     (mkdir-p out)
                     (call-with-output-file dummy
                       (const #t))))))
    (home-page #f)
    (synopsis #f)
    (description #f)
    (license (fsdg-compatible "dummy"))))

(define (%wsl-start-shepherd)
  "Start the system and shepherd.  This script takes the place the initrd,
makes the fixups for WSL, and launches the shepherd.  It is, for all intent
and purposes, the init for WSL.  It is *not* PID 1, however."
  (program-file
   "wsl-start-shepherd"
   (with-imported-modules '((guix build syscalls))
     #~(begin
         (use-modules (guix build syscalls))
         (let* ((system-generation
                 (readlink "/var/guix/profiles/system"))
                (system (readlink
                         (string-append
                          (if (absolute-file-name? system-generation)
                              ""
                              "/var/guix/profiles/")
                          system-generation))))

           ;; /run is mounted with the nosuid flag by WSL.  This prevents
           ;; running the /run/setuid-programs.  Remount it without this flag
           ;; as a workaround.  See:
           ;; https://github.com/microsoft/WSL/issues/8716.
           (mount #f "/run" #f
                  MS_REMOUNT
                  #:update-mtab? #f)

           ;;(format #t "Starting shepherd and al for GUIX_NEW_SYSTEM=~a" system)
           (setenv "GUIX_NEW_SYSTEM" system)
           ;; execl never returns.  Not sure if WSL expects this or not
           ;; through the [boot] command.  Ideally, we'd like to synchronize
           ;; on this command doing what it must before WSL launches the
           ;; shell.
           (execl #$(file-append guile-3.0 "/bin/guile")
                  "guile"
                  "--no-auto-compile"
                  (string-append system "/boot")))))))

(define %wsl-conf-file
  (mixed-text-file "wsl.conf"
"[user]
default = guest
[boot]
command = " (%wsl-start-shepherd) "\n"))

;; calling this function generates a gexp
(define (%install-wsl-conf bootloader)
  #~((mkdir-p "bin")
     (symlink #$(file-append bash "/bin/bash") "bin/sh")
     (symlink #$(file-append util-linux "/bin/mount") "bin/mount")
     (mkdir-p "etc")
     (symlink #$(bootloader-configuration-file bootloader) "etc/wsl.conf")))

;; Try to make this responsible for writing wsl.conf and linking /bin/mount
;; and /bin/sh inside the tarball.
(define wsl-bootloader
  (bootloader
   (name 'wsl-bootloader)
   (package dummy-package)
   (configuration-file %wsl-conf-file)
   (installer %install-wsl-conf)
   (configuration-file-generator
    (lambda (. _rest)
      (plain-file "dummy-bootloader" "")))
;;   (configuration-file-generator #f)
   (disk-image-installer #f)))

(define dummy-kernel dummy-package)

(define (dummy-initrd . _rest)
  (plain-file "dummy-initrd" ""))

(define-public wsl-os
  (operating-system
    (host-name "guix")
    (timezone "Etc/UTC")
    (bootloader
     (bootloader-configuration
      (bootloader wsl-bootloader)))
    (kernel dummy-kernel)
    (initrd dummy-initrd)
    (initrd-modules '())
    (firmware '())
    (file-systems '())
    (users (cons* (user-account
                   (name "guest")
                   (group "users")
                   ;; allow use of sudo
                   (supplementary-groups '("wheel"))
                   ;; password initially empty; type 'passwd' at the prompt to
                   ;; change your password and be able to sudo
                   (comment "Guest of GNU"))
                  %base-user-accounts))

    (packages
     (cons*
      ;; equip ourselves for some MS tutorials
      (specification->package "tmux")
      (specification->package "screen")
      ;; equip ourselves for some debugging within the WSL the kernel is
      ;; notorious for being quirky with some system calls.
      (specification->package "strace")
      ;; be able to communicate
      (specification->package "openssh")
      %base-packages))

    (services
     (cons*
      ;; these are requirements for WSL. They are setup in image.scm and need
      ;; to be maintained after proper system boot, so there.
      (extra-special-file "/etc/wsl.conf" %wsl-conf-file)
      (extra-special-file "/bin/sh" (file-append bash "/bin/bash"))
      (extra-special-file "/bin/mount" (file-append util-linux "/bin/mount"))

      (modify-services %base-services
        (guix-service-type
         config =>
         (guix-configuration
          (inherit config)
          (extra-options '("--disable-chroot"))))
        (delete virtual-terminal-service-type)
        (delete mingetty-service-type)
        (delete console-font-service-type))))))

(define wsl2-image
  (image
   (inherit
    (os->image wsl-os
               #:type wsl2-image-type))
   (name 'wsl2-image)))

wsl2-image
